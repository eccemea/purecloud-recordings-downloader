# PureCloud Recordings Downloader #

## Introduction ##

This application downloads PureCloud recordings using the PureCloud API

![Screenshot.jpg](https://bytebucket.org/eccemea/purecloud-recordings-downloader/raw/45bec9bf58b73033fab83a1ac436c70ae2009b0d/purecloud-recordings-downloader/images/Screenshot.jpg?token=f7d3a49ae9da72fca134a59b2809005690990a39)

## Usage ##

`purecloud.recordings.downloader.exe /d="<START DATE>" /de="<END DATE (OPTIONAL)>" /e=<PURECLOUD ENVIRONMENT> /i=<OAUTH CLIENT ID> /s=<OAUTH CLIENT SECRET> /o="<OUTPUT FOLDER>"`

## Parameters ##
* /d: Start Date, YYYY-MM-DD  HH:mm:ss (i.e. `2017-03-01` or `2017-03-01 15:00:00`)
* /de (optional): End Date, YYYY-MM-DD HH:mm:ss (i.e. `2017-03-02` or `2017-03-01 23:59:59`). By default the end date is the start date + 1 day - 1 tick
* /e: PureCloud Environment, mypurecloud.xxx (i.e. `mypurecloud.ie` for EMEA (Ireland), `mypurecloud.de` for Germany, `mypurecloud.com` for NA, `mypurecloud.jp` for JAPAN, `mypurecloud.com.au` for AUSTRALIA, etc.)
* /i: PureCloud OAuth Client Id, XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
* /s: PureCloud OAuth Client Secret: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
* /o: Output Folder. Any folder that can be accessed by the application
* /f: Output Format: webm (default), wav, mp3, wav_ulaw (.wav), ogg_vorbis (.org)

## Examples ##
`purecloud.recordings.downloader.exe /d="2017-03-01" /e=mypurecloud.ie /i=d5a89478-63e4-4f0b-9ff0-28cq36a017fd /s=j_oJ4PCWBJt6qADDBXbkiKYVQn8YKeZwA7vkXpjdyTU /o="C:\Downloads\Recordings"` will download all recordings for March 1st, 2017 to the "C:\Downloads\Recordings\YYYY\MM\DD" folder (YYYY, MM and DD folders are automatically created) in webm format (default)

`purecloud.recordings.downloader.exe /d="2017-03-01" /de="2017-03-10" /e=mypurecloud.ie /i=d5a89478-63e4-4f0b-9ff0-28cq36a017fd /s=j_oJ4PCWBJt6qADDBXbkiKYVQn8YKeZwA7vkXpjdyTU /o="C:\Downloads\Recordings" /f=mp3` will download all recordings between March 1st 2017 and March 10th 2017 in mp3 format

`purecloud.recordings.downloader.exe /d="2017-03-01 15:00:00" /de="2017-03-10 18:00:00" /e=mypurecloud.ie /i=d5a89478-63e4-4f0b-9ff0-28cq36a017fd /s=j_oJ4PCWBJt6qADDBXbkiKYVQn8YKeZwA7vkXpjdyTU /o="C:\Downloads\Recordings" /f=wav` will download all recordings for March 1st 2017 between 3 PM and 6 PM in .wav format

## File Format ##
Recordings are exported using the [webm](https://en.wikipedia.org/wiki/WebM) format with the following filename format: `<Conversation Id>.<Recording Id>.<MediaURI Key>.webm`

Example: `da4b1402-c34a-4b56-a3b0-05658dcb57b5.fe3c2513-c34a-4b56-a3b0-05658dcb68d5.0.webm`

Orphan recordings filename format: `Orphan.<Orphan Recording Id>.<Recording Id>.<MediaURI Key>.webm`

Example: `Orphan.da4b1402-c34a-4b56-a3b0-05658dcb57b5.fe3c2513-c34a-4b56-a3b0-05658dcb68d5.0.webm`

## Troubleshooting ##

Warning and Error messages are displayed in the console. ALL messages are also written to a log file in C:\Temp\Logs\YYYY-MM-DD.downloader.log

Note that the PureCloud API has [rate limits](https://developer.mypurecloud.com/api/rest/troubleshooting/rate_limits.html). This will cause the application to pause from time to time if too many requests are sent to PureCloud

### Status View ###

![StatusView.jpg](https://bytebucket.org/eccemea/purecloud-recordings-downloader/raw/ed183eb5756aab25de497bcd239062fbe7f3c657/purecloud-recordings-downloader/images/StatusView.jpg)

* Requeued: In PureCloud, recordings are transcoded on the fly. "Requeued" recordings will be retried later on.

## Getting Metadata ##

This application only downloads recordings (without the associated data). To get the metadata for each conversation, you need to use [PCSD](https://bitbucket.org/eccemea/purecloud-stats-dispatcher) with the following parameters:

`PCSD.exe /clientid=<CLIENT ID> /clientsecret=<CLIENT SECRET> /environment=<YOUR ENVIRONMENT> /target-csv=<OUTPUT FOLDER> /startdate=<SAME YYYY-MM-DD YOU USED TO DOWNLOAD RECORDINGS ABOVE>`
