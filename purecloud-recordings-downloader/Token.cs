﻿using System;

namespace purecloud.recordings.downloader
{
    internal class Token
    {
        public int Id { get; set; }
        public string SecurityToken { get; set; }
        public DateTime? ResetTime { get; set; }
        public bool Active => (!ResetTime.HasValue || ResetTime.Value < DateTime.Now) && !Booked;
        public bool Booked { get; set; } = false;
    }
}
