﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace purecloud.recordings.downloader
{
    internal class TokensWarehouse
    {
        private static readonly object SyncRoot = new object();
        private static List<Token> TokenList { get; } = new List<Token>();

        public static void Initialize(string environment, string clientId, string clientSecret, int tokensNumber)
        {
            var baseUrl = $"https://login.{environment}";
            var credentials = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes($"{clientId}:{clientSecret}"));
            var request = new RestRequest(new Uri($"{baseUrl}/oauth/token"), Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("Authorization", $"Basic {credentials}");
            request.AddParameter("grant_type", "client_credentials");
            var client = new RestClient(new Uri(baseUrl));

            lock (SyncRoot)
            {
                for (var i = 0; i < tokensNumber; i++)
                {
                    var response = client.Execute(request);
                    var token = JObject.Parse(response.Content).SelectToken("$.access_token").Value<string>();
                    TokenList.Add(new Token() { Id = i, SecurityToken = token });
                }
            }
        }

        public static Token GetToken()
        {
            lock (SyncRoot)
            {
                var token = TokenList.FirstOrDefault(x => x.Active);
                if (token == null) return null;
                token.Booked = true;
                return token;
            }
        }

        public static void RateLimitHasOccuredForToken(int id, DateTime? resetTime)
        {
            lock (SyncRoot)
            {
                var token = TokenList.First(x => x.Id.Equals(id));
                token.ResetTime = resetTime;
                token.Booked = false;
            }
        }
    }
}