﻿using System;
using System.Linq;

namespace purecloud.recordings.downloader
{
    internal class CommandLineArgs
    {
        public static string GetArgument(string name, string[] args)
        {
            try
            {
                return args.FirstOrDefault(x => x.StartsWith(name, StringComparison.InvariantCultureIgnoreCase))?.Split('=')[1];
            }
            catch
            {
                return string.Empty;
            }                        
        }
    }
}
