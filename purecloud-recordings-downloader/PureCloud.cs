using PureCloudPlatform.Client.V2.Api;
using PureCloudPlatform.Client.V2.Client;
using PureCloudPlatform.Client.V2.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace purecloud.recordings.downloader
{
  internal class PureCloud
  {
    private static string _environment;
    private static Token _currentToken;
    private static string _outputFolder;
    private static string _outputFormat;
    private static List<string> _outputFolderFiles = new List<string>();
    private static RecordingApi _recordingApi = new RecordingApi();

    private static List<AnalyticsConversation> _allConversations = new List<AnalyticsConversation>();
    private static Stack<string> _downloadQueue = new Stack<string>();
    private static List<OrphanRecording> _allOrphanRecordings = new List<OrphanRecording>();
    private static Stack<OrphanRecording> _orphanRecordingsDownloadQueue = new Stack<OrphanRecording>();

    private static int _recordingNotFound = 0;
    private static int _requeued = 0;

    #region Console Output

    private static void LogToConsole(string message, bool ownLine)
    {
      if (ownLine)
      {
        Console.WriteLine($"\n\r{message}");
        return;
      }
      var currentLineCursor = Console.CursorTop;
      Console.SetCursorPosition(0, Console.CursorTop);
      Console.Write(new string(' ', Console.WindowWidth));
      Console.SetCursorPosition(0, currentLineCursor);
      Console.Write($"\r{message}");
    }

    private static void ShowDownloadQueueStatus(string extraMessage = "")
    {
      LogToConsole($"- {_downloadQueue.Count}/{_allConversations.Count} downloads remaining (~{((_allConversations.Count - _downloadQueue.Count) * 100) / _allConversations.Count} %) - Requeued: {_requeued} - {extraMessage}", false);
    }

    private static void ShowOrphanRecordingsDownloadQueueStatus(string extraMessage = "")
    {
      LogToConsole($"- {_orphanRecordingsDownloadQueue.Count}/{_allOrphanRecordings.Count} orphan recordings downloads remaining (~{(_orphanRecordingsDownloadQueue.Count * 100) / _allOrphanRecordings.Count} %) - Requeued: {_requeued} - {extraMessage}", false);
    }

    #endregion

    #region Initialization

    public static void Initialize(string environment, string outputFolder, DateTime dateFrom, string outputFormat)
    {
      _environment = environment;
      _outputFolder = outputFolder;
      _outputFormat = outputFormat;

      if (!_outputFolder.EndsWith("\\")) _outputFolder += "\\";
      _outputFolder += $"{dateFrom.Year}\\{dateFrom.Month}\\{dateFrom.Day}\\";
      if (!Directory.Exists(_outputFolder)) Directory.CreateDirectory(_outputFolder);

      _outputFolderFiles = Directory.GetFiles(_outputFolder, "*.webm", SearchOption.AllDirectories).ToList();
      Console.WriteLine($"Output Folder (/o): {_outputFolder} ({_outputFolderFiles.Count} .webm files)");
    }

    public static void GetToken(DateTime? resetTime = null)
    {
      LogToConsole("Getting a token...", false);
      if (_currentToken != null) TokensWarehouse.RateLimitHasOccuredForToken(_currentToken.Id, resetTime);
      _currentToken = TokensWarehouse.GetToken();
      while (_currentToken == null)
      {
        Trace.Main.Info("No available tokens, waiting...");
        Thread.Sleep(1000);
        _currentToken = TokensWarehouse.GetToken();
      }

      Trace.Main.Info($"Next token, ID:{_currentToken.Id}, CODE:{_currentToken.SecurityToken}");
      Configuration.Default.ApiClient.RestClient.BaseUrl = new Uri($"https://api.{_environment}");
      Configuration.Default.AccessToken = _currentToken.SecurityToken;
    }

    public static void ResetProgressVariables()
    {
      _requeued = 0;
      _recordingNotFound = 0;
    }

    #endregion

    #region Get Conversations/Orphan Recordings

    public static void GetCallConversationData(Interval interval, int pageNumber = 1)
    {
      Trace.Main.Info($"GetCallConversationData({nameof(interval)}:{interval}, {nameof(pageNumber)}:{pageNumber})");
      LogToConsole($"Getting Call Conversation Data from {nameof(interval)}:{interval}, {nameof(pageNumber)}:{pageNumber}", false);
      try
      {
        var pageSize = 100;

        var api = new AnalyticsApi();
        var body = new ConversationQuery
        {
          Interval = interval.ToString(),
          Paging = new PagingSpec(pageSize, pageNumber),
          SegmentFilters = new List<AnalyticsQueryFilter>()
                    {
                        new AnalyticsQueryFilter(AnalyticsQueryFilter.TypeEnum.And, null, new List<AnalyticsQueryPredicate>() { new AnalyticsQueryPredicate() { _Operator = AnalyticsQueryPredicate.OperatorEnum.Matches, Dimension = AnalyticsQueryPredicate.DimensionEnum.Mediatype, Value = AnalyticsSession.MediaTypeEnum.Voice.ToString() } })
                        ,new AnalyticsQueryFilter(AnalyticsQueryFilter.TypeEnum.And, null, new List<AnalyticsQueryPredicate>() { new AnalyticsQueryPredicate() { _Operator = AnalyticsQueryPredicate.OperatorEnum.Matches, Dimension = AnalyticsQueryPredicate.DimensionEnum.Segmenttype, Value = "Interact" } })
                    },
          OrderBy = ConversationQuery.OrderByEnum.Conversationstart,
          Order = ConversationQuery.OrderEnum.Asc
        };

        var pageResult = api.PostAnalyticsConversationsDetailsQuery(body);
        while (pageResult?.Conversations != null && pageResult.Conversations.Any())
        {
          Trace.Main.Debug($"page no:{body.Paging.PageNumber}, items in page:{pageResult.Conversations.Count}");
          _allConversations.AddRange(pageResult.Conversations);
          LogToConsole($"Getting {pageResult.Conversations.Count} conversations (page #{body.Paging.PageNumber}). Total: {_allConversations.Count}", false);
          body.Paging.PageNumber++;
          pageResult = api.PostAnalyticsConversationsDetailsQuery(body);
        }
      }
      catch (ApiException ex)
      {
        switch (ex.ErrorCode)
        {
          case 401: // unauthorized
            GetToken();
            GetCallConversationData(interval);
            break;
          case 429: // API rate limit
            ex.Headers.TryGetValue("inin-ratelimit-count", out var ratelimitCount);
            ex.Headers.TryGetValue("inin-ratelimit-allowed", out var ratelimitAllowed);
            ex.Headers.TryGetValue("inin-ratelimit-reset", out var ratelimitReset);
            LogToConsole($"API rate limit has been reached, {nameof(ratelimitCount)}:{ratelimitCount}, {nameof(ratelimitAllowed)}:{ratelimitAllowed}, {nameof(ratelimitReset)}:{ratelimitReset}", false);
            int.TryParse(ratelimitReset, out var resetTimeSeconds);
            Thread.Sleep((resetTimeSeconds + 10) * 1000);
            GetCallConversationData(interval, pageNumber);
            break;
          default:
            throw;
        }
      }
      Trace.Main.Info($"{_allConversations.Count} conversations retrieved");
    }

    public static void AddConversationsToQueue()
    {
      foreach (var conversation in _allConversations)
      {
        _downloadQueue.Push(conversation.ConversationId);
      }
    }

    public static void GetOrphanRecordings()
    {
      Trace.Main.Info("\n\r");
      Trace.Main.Info("Getting orphan recordings");
      LogToConsole("Getting Orphan Recordings...", true);

      try
      {
        var pageNumber = 1;

        var pageResult = _recordingApi.GetOrphanrecordingsWithHttpInfo(100, pageNumber, "asc");
        while (pageResult?.Data != null && pageResult.Data.Entities.Any())
        {
          Trace.Main.Debug($"Orphan Recordings: page no:{pageNumber}, items in page:{pageResult.Data.Entities.Count}");
          _allOrphanRecordings.AddRange(pageResult.Data.Entities);
          LogToConsole($"Getting {pageResult.Data.Entities.Count} orphan recordings (page #{pageNumber}). Total: {_allOrphanRecordings.Count}", false);
          pageNumber++;
          pageResult = _recordingApi.GetOrphanrecordingsWithHttpInfo(100, pageNumber, "asc");
        }
      }
      catch (Exception ex)
      {
        Trace.Main.Error("Exception", ex);
      }
      Trace.Main.Info($"{_allOrphanRecordings.Count} orphan recordings retrieved");
    }

    public static void AddOrphanRecordingsToQueue()
    {
      foreach (var orphanRecording in _allOrphanRecordings)
      {
        _orphanRecordingsDownloadQueue.Push(orphanRecording);
      }
    }

    #endregion

    #region Download Recordings

    #region Conversation Recordings

    public static void ProcessDownloadQueue()
    {
      do
      {
        var conversationId = _downloadQueue.Pop();
        ShowDownloadQueueStatus(conversationId);

        // Has it been downloaded already?
        if (RecordingExists(conversationId))
        {
          Trace.Main.Info($"DownloadRecording({conversationId}): Recording for {conversationId} has probably already been downloaded");
          continue;
        }

        // Download recording(s)
        try
        {
          var recordings = _recordingApi.GetConversationRecordingsWithHttpInfo(conversationId, null, _outputFormat);
          if (recordings.StatusCode == 202) // Accepted (transcoding in progress)
          {
            RequeueDownloadRequest(conversationId);
            continue;
          }

          foreach (var recording in recordings.Data)
          {
            Trace.Main.Debug($"Conversation {conversationId} media URIs:{recording.MediaUris?.Count}");
            foreach (var mediaUri in recording.MediaUris ?? new Dictionary<string, MediaResult>())
            {
              DownloadManager.DownloadRecording(new Tuple<string, string>($"{conversationId}.{recording.Id}.{mediaUri.Key}", mediaUri.Value.MediaUri), _outputFolder, _outputFormat);
              recordings.Headers.TryGetValue("ININ-Correlation-Id", out var correlationIdOK);
              Trace.Main.Info($"DownloadRecording({conversationId}): OK. Correlation Id: {correlationIdOK}");
            }
          }
        }
        catch (ApiException ex)
        {
          switch (ex.ErrorCode)
          {
            case 401: // Unauthenticated
              GetToken();
              RequeueDownloadRequest(conversationId);
              break;
            case 403: // Unauthorized
              Trace.Main.Error("You are not authorized to download this recording.");
              break;
            case 404: // Not found                        
              Trace.Main.Warn($"DownloadRecording({conversationId}): No recordings found");
              _recordingNotFound++;
              break;
            case 415: // Unsupported media type. Ignore this recording.
              Trace.Main.Warn($"Unsupported media type error received for {conversationId}. Will ignore this recording.");
              break;
            case 429: // API rate limit
              ex.Headers.TryGetValue("inin-ratelimit-count", out var ratelimitCount);
              ex.Headers.TryGetValue("inin-ratelimit-allowed", out var ratelimitAllowed);
              ex.Headers.TryGetValue("inin-ratelimit-reset", out var ratelimitReset);
              Trace.Main.Info($"DownloadRecording({conversationId}): API rate limit has been reached, {nameof(ratelimitCount)}:{ratelimitCount}, {nameof(ratelimitAllowed)}:{ratelimitAllowed}, {nameof(ratelimitReset)}:{ratelimitReset}");
              ShowDownloadQueueStatus($"API Rate limit reached. Waiting {Convert.ToInt32(ratelimitReset) + 10} seconds...");
              int.TryParse(ratelimitReset, out var resetTimeSeconds);
              Thread.Sleep((resetTimeSeconds + 10) * 1000);
              RequeueDownloadRequest(conversationId);
              break;
            case 502: // Bad Gateway. Wait and try again.
              Trace.Main.Warn("Bad Gateway received (502). Need to wait and try again.");
              Thread.Sleep(60000);
              RequeueDownloadRequest(conversationId);
              break;
            case 503: // Gateway Timeout. Wait and try again.
              Trace.Main.Warn("Gateway timeout received (503). Need to wait and try again.");
              Thread.Sleep(60000);
              RequeueDownloadRequest(conversationId);
              break;
            case 504: // Request Timeout
              Trace.Main.Warn("Request timed out received (504). Need to wait and try again.");
              Thread.Sleep(60000);
              RequeueDownloadRequest(conversationId);
              break;
            default:
              Trace.Main.Error($"DownloadRecording({conversationId}): Exception.", ex);
              throw;
          }
        }
        catch (Exception ex)
        {
          Trace.Main.Error($"DownloadRecording({conversationId}): Exception.", ex);
          Console.WriteLine($"Exception: {ex}");
        }
        finally
        {
          ShowDownloadQueueStatus();
        }
      } while (_downloadQueue.Count > 0);
    }

    private static void RequeueDownloadRequest(string conversationId)
    {
      _requeued++;
      Task.Factory.StartNew(() =>
      {
        Thread.Sleep(60000);
        _requeued--;
        if (!_downloadQueue.Contains(conversationId))
        {
          _downloadQueue.Push(conversationId);
        }
      });
    }

    private static bool RecordingExists(string conversationId, string recordingId, string mediaUriKey)
    {
      var recordingFilename = Path.Combine(_outputFolder, $"{conversationId}.{recordingId}.{mediaUriKey}.webm");
      return _outputFolderFiles.Contains(recordingFilename);
    }

    private static bool RecordingExists(string conversationId)
    {
      return _outputFolderFiles.AsParallel().Where(f => f.StartsWith(Path.Combine(_outputFolder, conversationId))).ToList().Count > 0;
    }

    #endregion

    #region Orphan Recordings

    private static bool OrphanRecordingExists(string orphanRecordingId)
    {
      return _outputFolderFiles.AsParallel().Where(f => f.StartsWith(Path.Combine(_outputFolder, $"Orphan.{orphanRecordingId}"))).ToList().Count > 0;
    }

    private static void RequeueOrphanRecordingDownloadRequest(OrphanRecording orphanRecording)
    {
      _requeued++;
      Task.Factory.StartNew(() =>
      {
        Thread.Sleep(60000);
        _requeued--;
        if (!_orphanRecordingsDownloadQueue.Contains(orphanRecording))
        {
          _orphanRecordingsDownloadQueue.Push(orphanRecording);
        }
      });

    }

    public static void ProcessOrphanRecordingsDownloadQueue()
    {
      do
      {
        var orphanRecording = _orphanRecordingsDownloadQueue.Pop();
        ShowOrphanRecordingsDownloadQueueStatus($"Orphan {orphanRecording.Id}");

        // Has it been downloaded already?
        if (OrphanRecordingExists(orphanRecording.Id))
        {
          Trace.Main.Info($"DownloadOrphanRecording({orphanRecording.Id}): Orphan recording has already been downloaded");
          continue;
        }

        // Download orphan recordings
        try
        {
          Trace.Main.Debug($"Orphan Recording {orphanRecording.Id} media URIs:{orphanRecording.Recording?.MediaUris?.Count}");
          if (orphanRecording.Recording?.MediaUris != null)
          {
            foreach (var mediaUri in orphanRecording.Recording?.MediaUris)
            {
              var recordingFilename = Path.Combine(_outputFolder, $"Orphan.{orphanRecording.Id}.{orphanRecording.Recording.Id}.{mediaUri.Key}.webm");

              // Download orphan recording
              DownloadManager.DownloadRecording(new Tuple<string, string>($"Orphan.{orphanRecording.Id}.{orphanRecording.Recording.Id}.{mediaUri.Key}", mediaUri.Value.MediaUri), _outputFolder, _outputFormat);
            }
          }
        }
        catch (ApiException ex)
        {
          switch (ex.ErrorCode)
          {
            case 202: // Recording is still being transcoded. Need to try again later.
              var rec = (ApiResponse<List<Recording>>)ex.ErrorContent;
              var conversationIdRemaining = ex.Message;
              rec.Headers.TryGetValue("ININ-Correlation-Id", out var correlationId);
              Trace.Main.Warn($"DownloadOrphanRecording({orphanRecording.Id}): Recording is not available yet. Correlation Id: {correlationId}");
              break;
            case 401: // Unauthenticated
              GetToken();
              RequeueOrphanRecordingDownloadRequest(orphanRecording);
              break;
            case 403: // Unauthorized
              Trace.Main.Error("You are not authorized to download this recording.");
              break;
            case 404: // Not found                        
              Trace.Main.Warn($"DownloadOrphanRecording({orphanRecording.Id}): No recordings found");
              _recordingNotFound++;
              break;
            case 415: // Unsupported media type. Ignore this recording.
              Trace.Main.Warn($"Unsupported media type error received for {orphanRecording.Id}. Will ignore this recording.");
              break;
            case 429: // API rate limit
              ex.Headers.TryGetValue("inin-ratelimit-count", out var ratelimitCount);
              ex.Headers.TryGetValue("inin-ratelimit-allowed", out var ratelimitAllowed);
              ex.Headers.TryGetValue("inin-ratelimit-reset", out var ratelimitReset);
              Trace.Main.Info($"DownloadOrphanRecording({orphanRecording.Id}): API rate limit has been reached, {nameof(ratelimitCount)}:{ratelimitCount}, {nameof(ratelimitAllowed)}:{ratelimitAllowed}, {nameof(ratelimitReset)}:{ratelimitReset}");
              ShowOrphanRecordingsDownloadQueueStatus($"API Rate limit reached. Waiting {Convert.ToInt32(ratelimitReset) + 10} seconds...");
              int.TryParse(ratelimitReset, out var resetTimeSeconds);
              Thread.Sleep((resetTimeSeconds + 10) * 1000);
              RequeueOrphanRecordingDownloadRequest(orphanRecording);
              break;
            case 502: // Bad Gateway. Wait and try again.
              Trace.Main.Warn("Bad Gateway received (502). Need to wait and try again.");
              Thread.Sleep(60000);
              RequeueOrphanRecordingDownloadRequest(orphanRecording);
              break;
            case 503: // Gateway Timeout. Wait and try again.
              Trace.Main.Warn("Gateway timeout received (503). Need to wait and try again.");
              Thread.Sleep(60000);
              RequeueOrphanRecordingDownloadRequest(orphanRecording);
              break;
            case 504: // Request Timeout
              Trace.Main.Warn("Request timed out received (504). Need to wait and try again.");
              Thread.Sleep(60000);
              RequeueOrphanRecordingDownloadRequest(orphanRecording);
              break;
            default:
              Trace.Main.Error($"DownloadOrphanRecording({orphanRecording.Id}): Exception.", ex);
              throw;
          }
        }
        catch (Exception ex)
        {
          Trace.Main.Error($"DownloadOrphanRecording({orphanRecording.Id}): Exception.", ex);
          Console.WriteLine($"Exception: {ex}");
        }
        finally
        {
          ShowOrphanRecordingsDownloadQueueStatus();
        }
      } while (_orphanRecordingsDownloadQueue.Count > 0);
    }

    #endregion

    #endregion
  }
}
