using System;
using System.IO;
using System.Net;

namespace purecloud.recordings.downloader
{
  internal class DownloadManager
  {
    private static DateTime _lastStatsPrint = DateTime.Now;

    public static void DownloadRecording(Tuple<string, string> recordingUrl, string outputFolder, string outputFormat = "webm")
    {
      switch (outputFormat.ToLower())
      {
        case "wav":
        case "mp3":
        case "webm":
          // Nothing to do, all good here
          break;
        case "wav_ulaw":
          outputFormat = "wav";
          break;
        case "ogg_vorbis":
          outputFormat = "ogg";
          break;
        default:
          Trace.Main.Error($"Unknown output format: ${outputFormat}");
          break;
      }

      var webClient = new WebClient();
      if ((DateTime.Now - _lastStatsPrint).TotalSeconds > 10)
      {
        _lastStatsPrint = DateTime.Now;
      }
      Trace.Main.Debug($"Downloading {recordingUrl.Item2}...");
      try
      {
        var fullOutputFile = outputFolder + recordingUrl.Item1 + "." + outputFormat;
        if (File.Exists(fullOutputFile)) File.Delete(fullOutputFile);
        webClient.DownloadFile(recordingUrl.Item2, fullOutputFile);
      }
      catch (Exception ex)
      {
        Trace.Main.Error($"Download error: {recordingUrl.Item1}", ex);
      }
    }
  }
}
