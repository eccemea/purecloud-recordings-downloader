﻿using System;

namespace purecloud.recordings.downloader
{
    internal class Interval
    {        
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }

        public Interval(DateTime from, DateTime to)
        {            
            From = from;
            To = to;
        }

        public override string ToString()
        {
            if (From == null) throw new NullReferenceException($"{nameof(From)} field can't be NULL");
            if (To == null) throw new NullReferenceException($"{nameof(To)} field can't be NULL");
            var result = $"{From.Value:O}/{To.Value:O}";            
            if (From >= To) throw new Exception("Date TO must be grather than date FROM.");
            return result;
        }
    }
}
