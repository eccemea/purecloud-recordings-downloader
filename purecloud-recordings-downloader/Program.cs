using System;
using System.Diagnostics;

namespace purecloud.recordings.downloader
{
  class Program
  {
    private static Stopwatch _stopWatch = new Stopwatch();

    static void Main(string[] args)
    {
      try
      {
        Console.WriteLine();
        Console.WriteLine("=====================================");
        Console.WriteLine("=- PureCloud Recordings Downloader -=");
        Console.WriteLine("=====================================");
        Console.WriteLine();

        #region Parameters & Initialization

        Console.WriteLine("Parameters:");
        var dateFrom = DateTime.Parse(CommandLineArgs.GetArgument("/d", args));
        Console.WriteLine($"Date From (/d): {dateFrom}");
        DateTime dateTo;
        var dateToArgument = CommandLineArgs.GetArgument("/de", args);
        if (string.IsNullOrEmpty(dateToArgument))
        {
          dateTo = dateFrom.AddDays(1).AddTicks(-1);
          Console.WriteLine($"Date To (calculated): {dateTo}");
        }
        else
        {
          dateTo = DateTime.Parse(dateToArgument);
          Console.WriteLine($"Date To (/de): {dateTo}");
        }

        var environment = CommandLineArgs.GetArgument("/e", args);
        Console.WriteLine($"Environment (/e): {environment}");
        var clientId = CommandLineArgs.GetArgument("/i", args);
        Console.WriteLine($"Client Id (/i): {clientId}");
        var clientSecret = CommandLineArgs.GetArgument("/s", args);
        Console.WriteLine($"Client Secret (/s): **** (hidden)");
        var outputFolder = CommandLineArgs.GetArgument("/o", args);

        var outputFormat = CommandLineArgs.GetArgument("/f", args);
        Console.WriteLine($"Output Format (/f): {outputFormat}");

        PureCloud.Initialize(environment, outputFolder, dateFrom, outputFormat);

        Console.WriteLine(Environment.NewLine); // Double CR

        #endregion

        #region Initialization

        // Measure how long it takes to go through all recordings
        _stopWatch.Start();

        TokensWarehouse.Initialize(environment, clientId, clientSecret, 5);
        PureCloud.GetToken();

        #endregion

        #region Get Conversation Ids

        PureCloud.GetCallConversationData(new Interval(dateFrom, dateTo));
        PureCloud.AddConversationsToQueue();

        #endregion

        #region Download Recordings

        Console.WriteLine($"\n\rInitiating Recordings Download...");
        PureCloud.ProcessDownloadQueue();
        Console.WriteLine("\r\nFinished Downloading Recordings");

        #endregion

        #region Orphan Recordings

        PureCloud.ResetProgressVariables();
        PureCloud.GetOrphanRecordings();
        PureCloud.AddOrphanRecordingsToQueue();

        Console.WriteLine("\n\rInitiating Orphan Recordings Download...");
        PureCloud.ProcessOrphanRecordingsDownloadQueue();
        Console.WriteLine("\r\nFinished Downloading Orphan Recordings");

        #endregion
      }
      catch (Exception ex)
      {
        Trace.Main.Error("Exception in main():", ex);
        Console.WriteLine($"Exception: {ex}");
      }
      finally
      {
        _stopWatch.Stop();
        var ts = _stopWatch.Elapsed;

        Console.WriteLine();
        Trace.Main.Debug("Runtime:");
        Trace.Main.Debug($"Hours: {ts.Hours}");
        Trace.Main.Debug($"Minutes: {ts.Minutes}");
        Trace.Main.Debug($"Seconds: {ts.Seconds}");
        Trace.Main.Debug($"Milliseconds: {ts.Milliseconds}");
        Console.WriteLine($"Run time: {String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10)}");
        Console.WriteLine();
        Console.WriteLine("\n\r*** PRESS ANY KEY TO EXIT ***");
        Console.ReadKey();
      }
    }
  }
}
