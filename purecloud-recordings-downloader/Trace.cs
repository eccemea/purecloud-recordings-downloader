﻿using log4net;

namespace purecloud.recordings.downloader
{
    internal class Trace
    {
        public static readonly ILog Main = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }
}
